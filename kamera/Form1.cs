﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;

namespace kamera
{
    public partial class Form1 : Form
    {
        Bitmap cropBitmap;
        int cropX;
        int cropY;
        int cropWidth;
        int cropHeight;
        Pen cropPen;
        int cropPenSize = 2;
        System.Drawing.Drawing2D.DashStyle cropDashStyle = DashStyle.Solid;
        Color cropPenColor = Color.Aquamarine;
        Cursor c;
       
        public Form1()
        {
            InitializeComponent();
        }


        private void cmdStart_Click(object sender, EventArgs e)
        {
            // change the capture time frame
            this.WebCamCapture.TimeToCapture_milliseconds = (int)this.numCaptureTime.Value;

            // start the video capture. let the control handle the
            // frame numbers.
            this.WebCamCapture.Start(0);
        }


        private void WebCamCapture_ImageCaptured(object source, WebCam_Capture.WebcamEventArgs e)
        {
            // set the picturebox picture
            
            this.pictureBox1.Image = e.WebCamImage;
            
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //this.WebCamCapture.CaptureHeight = this.pictureBox1.Height;
            //this.WebCamCapture.CaptureWidth = this.pictureBox1.Width;
            pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
        }

        private void cmdStop_Click(object sender, EventArgs e)
        {
            // stop the video capture
            this.WebCamCapture.Stop();
            
        }

        private void cmdContinue_Click(object sender, EventArgs e)
        {
            // change the capture time frame
            this.WebCamCapture.TimeToCapture_milliseconds = (int)this.numCaptureTime.Value;

            // resume the video capture from the stop
            this.WebCamCapture.Start(this.WebCamCapture.FrameNumber);
        }

        private void pictureBox1_MouseUp(object sender, MouseEventArgs e)
        {
            try{           
            }
             catch (Exception exc)
            {
                MessageBox.Show(exc.Message, " Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }          
        }

        private void pictureBox1_MouseDown(object sender, MouseEventArgs e)
        {
            try{
                if(e.Button == System.Windows.Forms.MouseButtons.Left){
                        cropX = e.X;
                        cropY = e.Y;
                        cropPen = new Pen(cropPenColor, cropPenSize);
                        cropPen.DashStyle = cropDashStyle;
                }
                pictureBox1.Refresh();

            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, " Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
       
        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            try
            {
                if (pictureBox1.Image == null) { return; }
                if (e.Button == System.Windows.Forms.MouseButtons.Left)
                {
                    pictureBox1.Refresh();
                    cropWidth = e.X - cropX;
                    cropHeight = e.Y - cropY;
                    Graphics gg = pictureBox1.CreateGraphics();
                    gg.DrawRectangle(cropPen, cropX, cropY, cropWidth, cropHeight);

                }
                GC.Collect();
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, " Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnCrop_Click(object sender, EventArgs e)
        {
            try
            {
                if (cropWidth < 1) {
                    MessageBox.Show("You need to first select what portion of the image to crop.", " No cropping Cordinates!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                Rectangle rect = new Rectangle(cropX, cropY, cropWidth, cropHeight);
                Bitmap bit = new Bitmap(pictureBox1.Image, pictureBox1.Width, pictureBox1.Height);

                cropBitmap = new Bitmap(cropWidth, cropHeight);
                Graphics g = Graphics.FromImage(cropBitmap);
                g.DrawImage(bit, 0, 0, rect, GraphicsUnit.Pixel);
                pictureBox2.Image = cropBitmap;
            }
            catch (Exception exc)
            {
                MessageBox.Show(exc.Message, " Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            pictureBox2.Image.Save(Application.StartupPath + "\\crop.jpeg", ImageFormat.Jpeg);
        }

      
    }
}
